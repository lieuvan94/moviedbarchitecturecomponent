package com.lieunv.moviedb.core.architecture.usecase

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by LieuNV on 9/22/2020
 *
 * If Y want create something from base
 * Please code in here
 * UseCase<Type>
 */
abstract class UseCase<in Params,out T> where T :Any{

    private val compositeDisposable = CompositeDisposable()

    abstract fun createObservable(params: Params? = null): T

    protected fun subscribe(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    protected open fun handleError(throwable: Throwable): Throwable = throwable

    open fun onCleared() {
        compositeDisposable.clear()
    }

}