package com.lieunv.moviedb.core.architecture.view.adapter

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by LieuNV on 9/1/2020
 */
class BaseViewHolder<out T :ViewDataBinding> constructor(val binding: T) : RecyclerView.ViewHolder(binding.root)