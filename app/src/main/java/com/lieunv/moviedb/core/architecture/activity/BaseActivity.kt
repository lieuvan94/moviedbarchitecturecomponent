package com.lieunv.moviedb.core.architecture.activity

import android.content.Context
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.lieunv.moviedb.core.architecture.fragment.BaseFragment
import com.lieunv.moviedb.core.architecture.view.BaseView
import com.lieunv.moviedb.core.architecture.viewmodel.BaseViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Created by LieuNV on 8/31/2020
 */
abstract class BaseActivity <T: ViewDataBinding, V: BaseViewModel>: DaggerAppCompatActivity(),
    BaseView, BaseFragment.FragmentCallBack {

    private lateinit var baseViewDataBinding: T

    private lateinit var baseViewModel: V

    protected fun getBaseViewDataBinding(): T = baseViewDataBinding

    abstract fun getBaseViewModel(): V

    abstract fun getBindingVariable(): Int

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
        initView()
        initAction()
    }

    override fun hideKeyBoard() {
        val view = this.currentFocus
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    override fun showLoading(isShow: Boolean) {
        TODO("Not yet implemented")
    }

    override fun showToast(message :String){
        try {
            Toast.makeText(applicationContext,message,Toast.LENGTH_LONG).show()
        }catch (ignored : Exception){
        }
    }


    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }

    private fun performDataBinding() {
        baseViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        baseViewModel = getBaseViewModel()
        baseViewDataBinding.setVariable(getBindingVariable(), baseViewModel)
        baseViewDataBinding.executePendingBindings()
    }

}