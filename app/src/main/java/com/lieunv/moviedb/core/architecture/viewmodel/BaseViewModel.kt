package com.lieunv.moviedb.core.architecture.viewmodel

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by LieuNV on 8/31/2020
 */
abstract class BaseViewModel : ViewModel() {

    protected var compositeDisposable = CompositeDisposable()


    override fun onCleared() {
        compositeDisposable.clear()
        compositeDisposable.dispose()
        super.onCleared()
    }

}