package com.lieunv.moviedb.core.di.component

/**
 * Created by LieuNV on 8/31/2020
 */
interface BaseInjectorComponent {
    fun inject(target: Any)
}