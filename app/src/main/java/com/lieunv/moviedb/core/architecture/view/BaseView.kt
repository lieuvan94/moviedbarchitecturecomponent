package com.lieunv.moviedb.core.architecture.view

import androidx.annotation.LayoutRes

/**
 * Created by LieuNV on 8/31/2020
 */
interface BaseView{

    @LayoutRes
    fun getLayoutId() : Int

    fun initView()

    fun initAction()

    fun hideKeyBoard()

    fun showLoading(isShow: Boolean)

    fun showToast(message: String)
}