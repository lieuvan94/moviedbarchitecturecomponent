package com.lieunv.moviedb.core.architecture.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.lieunv.moviedb.core.architecture.activity.BaseActivity
import com.lieunv.moviedb.core.architecture.view.BaseView
import com.lieunv.moviedb.core.architecture.viewmodel.BaseViewModel
import com.lieunv.moviedb.core.di.Injectable
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Created by LieuNV on 8/31/2020
 */
abstract class BaseFragment<T : ViewDataBinding, V: BaseViewModel> : DaggerFragment(),
    BaseView, Injectable {

    private lateinit var baseViewModel: V

    private lateinit var baseViewDataBinding: T

    protected fun getBaseViewDataBinding(): T = baseViewDataBinding

    abstract fun getBaseViewModel(): V

    abstract fun getBindingVariable(): Int

    abstract fun initData(argument: Bundle?)

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context as? BaseActivity<*, *>)?.onFragmentAttached()
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        baseViewDataBinding =
            DataBindingUtil.inflate(inflater,getLayoutId(),container,false)

        return baseViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        baseViewModel = getBaseViewModel()
        baseViewDataBinding.setVariable(getBindingVariable(),baseViewModel)
        baseViewDataBinding.lifecycleOwner = viewLifecycleOwner
        baseViewDataBinding.executePendingBindings()

        initView()
        initAction()
        initData(arguments)
    }

    override fun hideKeyBoard() {
        TODO("Not yet implemented")
    }

    override fun showLoading(isShow: Boolean) {
        TODO("Not yet implemented")
    }

    override fun showToast(message: String) {
        try {
            (context as? BaseActivity<*, *>)?.showToast(message)
        } catch (ignored: Exception) {
        }
    }

    interface FragmentCallBack{
        fun onFragmentAttached()

        fun onFragmentDetached(tag: String)
    }
}