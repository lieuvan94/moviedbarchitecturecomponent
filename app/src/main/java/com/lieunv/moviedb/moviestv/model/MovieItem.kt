package com.lieunv.moviedb.moviestv.model

import com.lieunv.moviedb.moviestv.data.remote.response.MoviesResponse

/**
 * Created by LieuNV on 9/28/2020
 */
data class MovieItem(
    val popularity : Double,
    val voteCount : Int,
    val video : Boolean,
    val posterPath : String,
    val id : Int,
    val adult : Boolean,
    val backdropPath : String,
    val originalLanguage : String,
    val originalTitle : String,
    val genreIds : List<Int>,
    val title : String,
    val voteAverage : Double,
    val overview : String,
    val releaseDate : String
): ItemModel()

fun List<MoviesResponse>.toMovieItem() : List<MovieItem> =
    map {
        MovieItem(
            popularity = it.popularity,
            voteCount = it.voteCount,
            video = it.video,
            posterPath = it.posterPath,
            id = it.id,
            adult = it.adult,
            backdropPath = it.backdropPath,
            originalLanguage = it.originalLanguage,
            originalTitle = it.originalTitle,
            genreIds = it.genreIds,
            title = it.title,
            voteAverage = it.voteAverage,
            overview = it.overview,
            releaseDate = it.releaseDate
        )
    }