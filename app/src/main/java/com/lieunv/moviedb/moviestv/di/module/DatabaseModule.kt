package com.lieunv.moviedb.moviestv.di.module

import android.app.Application
import androidx.room.Room
import com.lieunv.moviedb.moviestv.data.local.dao.MovieDao
import com.lieunv.moviedb.moviestv.data.local.database.AppDatabase
import com.lieunv.moviedb.moviestv.utils.constants.DatabaseConstants
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by LieuNV on 9/24/2020
 */
@Module
open class DatabaseModule {
    companion object{
        val instance = DatabaseModule()
    }

    @Singleton
    @Provides
    open fun provideDb(app: Application): AppDatabase =
        Room.databaseBuilder(app, AppDatabase::class.java, DatabaseConstants.DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()

    @Provides
    @Singleton
    fun provideMovieDao(db: AppDatabase): MovieDao {
        return db.movieDao()
    }
}