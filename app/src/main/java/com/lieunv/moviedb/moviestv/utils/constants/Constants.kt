package com.lieunv.moviedb.moviestv.utils.constants

/**
 * Created by LieuNV on 9/22/2020
 */
object Constants {
    const val MOVIE_POPULAR = "popular"
    const val MOVIE_NOW_PLAYING = "now_playing"
    const val MOVIE_UPCOMING = "upcoming"
    const val MOVIE_TOP_RATES = "top_rated"

}