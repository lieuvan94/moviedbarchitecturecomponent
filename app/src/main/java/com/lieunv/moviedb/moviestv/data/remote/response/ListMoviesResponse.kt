package com.lieunv.moviedb.moviestv.data.remote.response

import com.google.gson.annotations.SerializedName

/**
 * Created by LieuNV on 9/28/2020
 */
data class ListMoviesResponse(
    @SerializedName("results") val results : List<MoviesResponse>,
    @SerializedName("page") val page : Int,
    @SerializedName("total_results") val totalResults : Int,
    @SerializedName("total_pages") val totalPages : Int
) : ResponseModel()