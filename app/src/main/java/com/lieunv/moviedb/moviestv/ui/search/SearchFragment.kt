package com.lieunv.moviedb.moviestv.ui.search

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.lieunv.moviedb.BR
import com.lieunv.moviedb.R
import com.lieunv.moviedb.core.architecture.fragment.BaseFragment
import com.lieunv.moviedb.databinding.FragmentSearchBinding


/**
 * Created by LieuNV on 9/24/2020
 */
class SearchFragment : BaseFragment<FragmentSearchBinding,SearchViewModel>(){
    private lateinit var searchViewModel: SearchViewModel

    override fun getBaseViewModel(): SearchViewModel {
        searchViewModel = ViewModelProvider(this,viewModelFactory).get(SearchViewModel::class.java)
        return searchViewModel
    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun initData(argument: Bundle?) {

    }

    override fun getLayoutId(): Int = R.layout.fragment_search

    override fun initView() {

    }

    override fun initAction() {

    }


}