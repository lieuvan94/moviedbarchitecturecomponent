package com.lieunv.moviedb.moviestv.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.lieunv.moviedb.core.architecture.viewmodel.BaseViewModel
import com.lieunv.moviedb.core.utils.ext.addToCompositeDisposable
import com.lieunv.moviedb.core.utils.ext.applyScheduler
import com.lieunv.moviedb.moviestv.data.remote.onErrorResponse
import com.lieunv.moviedb.moviestv.model.MovieItem
import com.lieunv.moviedb.moviestv.model.toMovieItem
import com.lieunv.moviedb.moviestv.repository.MoviesRepository
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

/**
 * Created by LieuNV on 9/22/2020
 */
class HomeViewModel @Inject constructor(
    private val moviesRepository: MoviesRepository
): BaseViewModel() {

    val imageUrl = "https://image.tmdb.org/t/p/w500/6CoRTJTmijhBLJTUNoVSUNxZMEI.jpg"

    val moviesLiveData : LiveData<ArrayList<MovieItem>> by lazy { _moviesMutableLiveData }
    private val _moviesMutableLiveData = MutableLiveData<ArrayList<MovieItem>>()

    fun getMoviesByCategory(category: String, page: Int) {
        moviesRepository.getMoviesByCategory(category, page)
            .applyScheduler()
            .subscribeBy(
                onSuccess = {listMoviesResponse ->
                    _moviesMutableLiveData.postValue(listMoviesResponse.data.results.toMovieItem() as ArrayList<MovieItem>?)
                },
                onError = onErrorResponse {}
            ).addToCompositeDisposable(compositeDisposable)
    }
}