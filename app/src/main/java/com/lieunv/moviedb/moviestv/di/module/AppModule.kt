package com.lieunv.moviedb.moviestv.di.module

import android.app.Application
import android.content.Context
import com.lieunv.moviedb.core.architecture.repository.Repository
import com.lieunv.moviedb.core.utils.rx.AppSchedulerProvider
import com.lieunv.moviedb.core.utils.rx.SchedulerProvider
import com.lieunv.moviedb.moviestv.data.remote.service.MoviesApi
import com.lieunv.moviedb.moviestv.repository.MoviesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by LieuNV on 9/22/2020
 */
@Module
internal object AppModule {

    @Singleton
    @Provides
    @JvmStatic
    fun provideContext(application: Application): Context = application

    @Singleton
    @Provides
    @JvmStatic
    fun provideSchedulerProvider(): SchedulerProvider = AppSchedulerProvider()

    @Singleton
    @Provides
    @JvmStatic
    fun provideMoviesRepository(moviesApi: MoviesApi): Repository = MoviesRepository(moviesApi)

}