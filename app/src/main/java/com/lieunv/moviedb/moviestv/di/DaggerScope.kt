package com.lieunv.moviedb.moviestv.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ExampleScope