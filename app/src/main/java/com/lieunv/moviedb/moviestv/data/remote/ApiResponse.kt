package com.lieunv.moviedb.moviestv.data.remote

import com.google.gson.annotations.SerializedName
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException
import java.net.SocketTimeoutException

/**
 * API response
 */
data class ApiResponse<T>(
    @SerializedName("code")
    val code: String,
    @SerializedName("data")
    val data: T,
    @SerializedName("message")
    val message: String = ""
)

/**
 * HTTP error conversion
 */
data class ErrorResponse(
    @SerializedName("code")
    val code: String,
    @SerializedName("message")
    val message: String
)

enum class ApiResponseCode(val value: String) {
    OK("OK"),
    TOKEN_INVALID("E_TokenInvalid"),
    MISSING_REQUIRED_PARAM("E_MissingRequiredParam"),
    ;

    companion object {
        fun errorCodeFromMessage(message: String) = values().find { it.value == message }
    }
}

/**
 * Error response processing
 *
 * @param onFailure
 */
fun onErrorResponse(onFailure: () -> Unit): (Throwable) -> Unit =
    fun(error: Throwable) {
        when (error) {
            is HttpException -> Timber.e("Http Exception : code = ${error.response()?.code()}")
            is SocketTimeoutException -> Timber.e("time out")
            is IOException -> Timber.e("network error")
            else -> Timber.e(error)
        }
        onFailure()
    }
