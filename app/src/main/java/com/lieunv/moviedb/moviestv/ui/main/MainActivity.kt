package com.lieunv.moviedb.moviestv.ui.main

import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.lieunv.moviedb.BR
import com.lieunv.moviedb.R
import com.lieunv.moviedb.core.architecture.activity.BaseActivity
import com.lieunv.moviedb.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    private lateinit var mainViewModel: MainViewModel

    override fun getBaseViewModel(): MainViewModel {
        mainViewModel = ViewModelProvider(this,viewModelFactory).get(MainViewModel::class.java)
        return mainViewModel
    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun initView() {
       setupBottomNavigation()
    }

    override fun initAction() {

    }

    /**
     *  Show bottomNavigation
     */
    fun showBottomNavigation(){
        getBaseViewDataBinding().bottomNavigation.visibility = View.VISIBLE
    }

    /**
     * hide bottomNavigation
     */
    fun hideBottomNavigation(){
        getBaseViewDataBinding().bottomNavigation.visibility = View.GONE
    }

    /**
     *  setup bottomNavigation
     */
    private fun setupBottomNavigation(){
        getBaseViewDataBinding().bottomNavigation.apply {
            setupWithNavController(
                Navigation.findNavController(
                    this@MainActivity,
                    R.id.contentFrame
                )
            )

            setOnNavigationItemReselectedListener {
                //No processing when reselecting
            }

            setOnNavigationItemSelectedListener {
                NavigationUI.onNavDestinationSelected(it,
                    Navigation.findNavController(
                    this@MainActivity,
                    R.id.contentFrame
                ))
                false
            }
        }
    }

}