package com.lieunv.moviedb.moviestv.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lieunv.moviedb.core.di.BaseViewModelFactory
import com.lieunv.moviedb.core.di.ViewModelKey
import com.lieunv.moviedb.moviestv.ui.home.HomeViewModel
import com.lieunv.moviedb.moviestv.ui.main.MainViewModel
import com.lieunv.moviedb.moviestv.ui.search.SearchViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by LieuNV on 9/23/2020
 */
@Module
interface ViewModelModule {

    @Binds
    fun bindViewModelFactory(factory: BaseViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun binMainViewModel(
        mainViewModel: MainViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    fun binHomeViewModel(
        homeViewModel: HomeViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    fun binSearchViewModel(
        searchViewModel: SearchViewModel
    ): ViewModel

}