package com.lieunv.moviedb.moviestv.data.mapper

import com.lieunv.moviedb.moviestv.data.local.entity.EntityModel
import com.lieunv.moviedb.moviestv.data.remote.response.ResponseModel
import com.lieunv.moviedb.moviestv.model.ItemModel

/**
 * Created by LieuNV on 9/24/2020
 */
interface ItemMapper<RM : ResponseModel, IM : ItemModel> {
    fun mapToItem(responseModel: RM): IM

    fun mapToResponse(itemModel: IM): RM
}

interface ResponseMapper<RM : ResponseModel, EM : EntityModel> {
    fun mapToEntity(responseModel: RM): EM

    fun mapToResponse(entityModel: EM): RM
}

interface EntityMapper<EM : EntityModel, IM : ItemModel> {
    fun mapToItem(entityModel: EM): IM

    fun mapToEntity(itemModel: IM): EM
}