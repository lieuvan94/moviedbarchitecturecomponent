package com.lieunv.moviedb.moviestv.di.module

import com.lieunv.moviedb.moviestv.ui.home.HomeFragment
import com.lieunv.moviedb.moviestv.ui.search.SearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by LieuNV on 9/23/2020
 */
@Module
interface FragmentModule {
    @ContributesAndroidInjector
    fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    fun contributeSearchFragment(): SearchFragment
}