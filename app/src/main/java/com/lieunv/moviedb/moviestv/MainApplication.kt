package com.lieunv.moviedb.moviestv

import com.lieunv.moviedb.BuildConfig
import com.lieunv.moviedb.moviestv.di.component.DaggerAppComponent
import com.lieunv.moviedb.moviestv.di.module.DatabaseModule
import com.lieunv.moviedb.moviestv.di.module.NetworkModule
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber
import javax.inject.Singleton

/**
 * Created by LieuNV on 9/23/2020
 */
@Singleton
class MainApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder()
            .application(this)
            .databaseModule(DatabaseModule.instance)
            .networkModule(NetworkModule.instance)
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        // Timber initialization
        if (BuildConfig.DEBUG) {
            // debug at the time of output to the local
            Timber.plant(Timber.DebugTree())
        }

        // Refer to the following url to handle UndeliverableException of RxJava
        // https://qiita.com/fukasawah/items/71b8f5931265fd384231
        RxJavaPlugins.setErrorHandler {
            Timber.e("${Thread.currentThread().name}: catch GlobalErrorHandler ${it.message}")
        }
    }
}