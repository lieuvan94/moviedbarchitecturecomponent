package com.lieunv.moviedb.moviestv.data.local.entity

import androidx.room.Entity

/**
 * Created by LieuNV on 9/24/2020
 */
@Entity(tableName = "movie_tb",primaryKeys = ["name"])
class MovieEntity(
    val name :String = ""
): EntityModel()