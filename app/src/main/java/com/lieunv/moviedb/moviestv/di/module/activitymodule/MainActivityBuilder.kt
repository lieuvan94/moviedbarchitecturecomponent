package com.lieunv.moviedb.moviestv.di.module.activitymodule

import com.lieunv.moviedb.moviestv.di.module.FragmentModule
import com.lieunv.moviedb.moviestv.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by LieuNV on 9/23/2020
 */
@Module
interface MainActivityBuilder {
    @ContributesAndroidInjector(
        modules = [
            MainActivityModule::class,
            FragmentModule::class
        ]
    )
    fun contributeMainActivity(): MainActivity
}