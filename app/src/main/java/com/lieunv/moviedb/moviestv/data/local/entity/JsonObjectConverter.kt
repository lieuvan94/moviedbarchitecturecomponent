package com.lieunv.moviedb.moviestv.data.local.entity

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

/**
 * Convert Json Object to Map <String, String> format
 */
object JsonObjectConverter {
    @TypeConverter
    @JvmStatic
    fun fromString(value: String): Map<String, String> = run {
        val mapType: Type = object : TypeToken<Map<String, String>>() {}.type
        Gson().fromJson(value, mapType)
    }

    @TypeConverter
    @JvmStatic
    fun fromStringMap(map: Map<String, String>): String = Gson().toJson(map)
}