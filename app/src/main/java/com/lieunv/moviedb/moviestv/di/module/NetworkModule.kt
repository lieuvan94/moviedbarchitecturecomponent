package com.lieunv.moviedb.moviestv.di.module

import android.content.Context
import com.lieunv.moviedb.BuildConfig
import com.lieunv.moviedb.moviestv.data.remote.service.MoviesApi
import com.lieunv.moviedb.moviestv.utils.constants.APIConstants
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by LieuNV on 9/24/2020
 */
@Module
open class NetworkModule {

    companion object{
        val instance = NetworkModule()
    }

    private fun provideBaseURL(): String = BuildConfig.BASE_URL

    @Singleton
    @Provides
    fun provideCache(context: Context): Cache{
        val cacheSize = 5 * 1024 * 1024 // 5 MB
        val cacheDir = context.cacheDir
        return Cache(cacheDir, cacheSize.toLong())
    }

    @Singleton
    @Provides
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Singleton
    @Provides
    fun provideRequestInterceptor (): Interceptor{

        return Interceptor { chain ->
            val original: Request = chain.request()
            val originalUrl = original.url()
            val url = originalUrl.newBuilder()
                .addQueryParameter(
                    APIConstants.QUERY_API_KEY,
                    APIConstants.TMDB_API_KEY
                )
                .build()

            val requestBuilder: Request.Builder = original.newBuilder()
                .url(url)
            val request: Request = requestBuilder.build()
            chain.proceed(request)
        }

    }


    @Singleton
    @Provides
    fun provideOkHttpClient(cache: Cache,interceptor: HttpLoggingInterceptor, requestInterceptor: Interceptor) : OkHttpClient{
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(APIConstants.TIMEOUT_IN_MS,TimeUnit.MILLISECONDS)
        builder.readTimeout(APIConstants.TIMEOUT_IN_MS,TimeUnit.MILLISECONDS)
        builder.writeTimeout(APIConstants.TIMEOUT_IN_MS,TimeUnit.MILLISECONDS)
        builder.cache(cache)
        builder.addInterceptor(interceptor)
        builder.addInterceptor(requestInterceptor)

        return builder.build()

    }

    @Singleton
    @Provides
    fun provideRetrofit(client: OkHttpClient): Retrofit{
        return  Retrofit.Builder()
            .baseUrl(provideBaseURL())
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun provideMoviesApi(retrofit: Retrofit): MoviesApi {
        return retrofit.create(MoviesApi::class.java)
    }

}