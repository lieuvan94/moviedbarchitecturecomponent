package com.lieunv.moviedb.moviestv.repository

import com.lieunv.moviedb.core.architecture.repository.Repository
import com.lieunv.moviedb.moviestv.data.remote.ApiResponse
import com.lieunv.moviedb.moviestv.data.remote.response.ListMoviesResponse
import com.lieunv.moviedb.moviestv.data.remote.service.MoviesApi
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by LieuNV on 9/22/2020
 */
class MoviesRepository @Inject constructor(
    private val remoteDataSource: MoviesApi
) : Repository {

    fun getMoviesByCategory(category: String, page: Int): Single<ApiResponse<ListMoviesResponse>> =
        remoteDataSource.getMoviesByCategory(category, page)

}