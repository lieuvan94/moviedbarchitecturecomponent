package com.lieunv.moviedb.moviestv.ui.common

import android.widget.ImageButton
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingMethod
import androidx.databinding.BindingMethods
import com.bumptech.glide.Glide

/**
 * Created by LieuNV on 9/25/2020
 */
class CustomBindingAdapter {
    @BindingMethods(
        BindingMethod(
            type = android.widget.ImageButton::class,
            attribute = "srcCompat",
            method = "setImageDrawable"
        ),
        BindingMethod(
            type = android.widget.ImageView::class,
            attribute = "srcCompat",
            method = "setImageResource"
        )
    )
    object BindingAdapters {

        @BindingAdapter("imageUrl")
        @JvmStatic
        fun imageUrl(imageView: ImageView, url: String?) {

            if (!url.isNullOrEmpty()) {
                Glide.with(imageView.context)
                    .load(url)
                    .fitCenter()
                    .into(imageView)
            }
        }

        @BindingAdapter("imageUrl", "error")
        @JvmStatic
        fun imageUrl(imageView: ImageView, url: String?, error: Int) {
            if (url.isNullOrEmpty()) {
                imageView.setImageResource(error)
                return
            }

            Glide.with(imageView.context)
                .load(url)
                .error(error)
                .fitCenter()
                .into(imageView)
        }

        @BindingAdapter("imageSrcGif")
        @JvmStatic
        fun imageSrcGif(imageViewGif: ImageView, resId: Int) {
            Glide.with(imageViewGif.context)
                .load(resId)
                .centerInside()
                .into(imageViewGif)
        }

    }
}