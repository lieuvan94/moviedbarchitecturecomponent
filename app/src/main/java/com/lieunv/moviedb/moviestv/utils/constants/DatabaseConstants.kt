package com.lieunv.moviedb.moviestv.utils.constants

/**
 * Created by LieuNV on 9/24/2020
 */
object DatabaseConstants {
    const val DATABASE_VERSION = 1
    const val DATABASE_NAME = "movies.db"
}