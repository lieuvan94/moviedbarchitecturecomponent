package com.lieunv.moviedb.moviestv.ui.home

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.lieunv.moviedb.BR
import com.lieunv.moviedb.R
import com.lieunv.moviedb.core.architecture.fragment.BaseFragment
import com.lieunv.moviedb.databinding.FragmentHomeBinding

/**
 * Created by LieuNV on 9/22/2020
 */
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    private lateinit var homeViewModel: HomeViewModel

    override fun getBaseViewModel(): HomeViewModel {
        homeViewModel = ViewModelProvider(this,viewModelFactory).get(HomeViewModel::class.java)
        return homeViewModel
    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun initData(argument: Bundle?) {

    }

    override fun getLayoutId(): Int = R.layout.fragment_home

    override fun initView() {

    }

    override fun initAction() {

    }

}