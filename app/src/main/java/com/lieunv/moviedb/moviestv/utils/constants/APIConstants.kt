package com.lieunv.moviedb.moviestv.utils.constants

/**
 * Created by LieuNV on 9/24/2020
 */
object APIConstants {
    const val TIMEOUT_IN_MS = 30000L
    const val BASE_SERVER_URL = "https://api.themoviedb.org/3/"
    const val TMDB_API_KEY = "2f5c0a189c992ac7896d296f837ab5e1"
    const val QUERY_API_KEY = "api_key"
    const val POST_PATH_URL = "https://image.tmdb.org/t/p/w500"
}