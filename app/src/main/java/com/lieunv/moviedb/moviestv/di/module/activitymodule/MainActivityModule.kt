package com.lieunv.moviedb.moviestv.di.module.activitymodule

import androidx.appcompat.app.AppCompatActivity
import com.lieunv.moviedb.moviestv.ui.main.MainActivity
import dagger.Binds
import dagger.Module

/**
 * Created by LieuNV on 9/23/2020
 */
@Module
interface MainActivityModule {
    @Binds
    fun providesMainActivity(activity: MainActivity): AppCompatActivity
}