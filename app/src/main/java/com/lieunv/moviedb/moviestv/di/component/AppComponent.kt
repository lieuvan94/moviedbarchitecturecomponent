package com.lieunv.moviedb.moviestv.di.component

import android.app.Application
import com.lieunv.moviedb.moviestv.MainApplication
import com.lieunv.moviedb.moviestv.di.module.AppModule
import com.lieunv.moviedb.moviestv.di.module.DatabaseModule
import com.lieunv.moviedb.moviestv.di.module.NetworkModule
import com.lieunv.moviedb.moviestv.di.module.ViewModelModule
import com.lieunv.moviedb.moviestv.di.module.activitymodule.MainActivityBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by LieuNV on 9/22/2020
 */
@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        DatabaseModule::class,
        NetworkModule::class,
        MainActivityBuilder::class,
        ViewModelModule::class
    ]
)
interface AppComponent : AndroidInjector<MainApplication> {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application): Builder
        fun networkModule(networkModule: NetworkModule): Builder
        fun databaseModule(databaseModule: DatabaseModule): Builder
        fun build(): AppComponent

    }

    override fun inject(instance: MainApplication)
}