package com.lieunv.moviedb.moviestv.data.remote.service

import com.lieunv.moviedb.moviestv.data.remote.ApiResponse
import com.lieunv.moviedb.moviestv.data.remote.response.ListMoviesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Singleton

/**
 * Created by LieuNV on 9/24/2020
 */
@Singleton
interface MoviesApi {

    @GET("movie/{category}")
    fun getMoviesByCategory(
        @Path("category") category: String,
        @Query("page") page :Int
    ):Single<ApiResponse<ListMoviesResponse>>
}