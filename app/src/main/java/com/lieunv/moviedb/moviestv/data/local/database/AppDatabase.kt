package com.lieunv.moviedb.moviestv.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.lieunv.moviedb.moviestv.data.local.dao.MovieDao
import com.lieunv.moviedb.moviestv.data.local.entity.MovieEntity
import com.lieunv.moviedb.moviestv.utils.constants.DatabaseConstants

/**
 * Created by LieuNV on 9/24/2020
 */
@Database(
    entities = [
        MovieEntity::class
    ],
    version = DatabaseConstants.DATABASE_VERSION,
    exportSchema = false
)
abstract class AppDatabase: RoomDatabase() {
    abstract fun movieDao(): MovieDao
}