package com.lieunv.moviedb.moviestv.data.remote

/**
 * Created by LieuNV on 9/24/2020
 */
enum class Status {
    LOADING,
    SUCCESS,
    FAILED
}
class NetworkState(val status:Status, val msg: String) {
    companion object {
        val LOADED: NetworkState
        val LOADING: NetworkState
        val ERROR: NetworkState
        init {
            LOADED = NetworkState(Status.SUCCESS, "Success")
            LOADING = NetworkState(Status.LOADING, "Loading")
            ERROR = NetworkState(Status.FAILED, "Có lỗi xảy ra, vui lòng thử lại.")
        }
    }
}